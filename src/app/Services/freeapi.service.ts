import { from, Observable } from "rxjs";
import {Injectable} from '@angular/core'
import { HttpClient,HttpParams } from '@angular/common/http';
import { Posts} from'../classes/posts';
import { Transactions} from'../classes/transactions';
@Injectable()
export class freeApiService
{
    
    constructor(private http:HttpClient){};
    getcomments():Observable<any>{
        return this.http.get("https://jsonplaceholder.typicode.com/posts/1/comments")
        }
        getcommentsbyparameter():Observable<any>
        {
            let params1 = new HttpParams().set('userId',"1");
            return this.http.get("https://jsonplaceholder.typicode.com/posts",{params:params1})
        }
        post(opost:Posts): Observable<any>
        {
            return this.http.post("https://jsonplaceholder.typicode.com/posts",opost)
        }
        post1(transactionobj:Transactions): Observable<any>
        {
            return this.http.post("https://us-central1-creditapp-29bf2.cloudfunctions.net/new_node_bigquery_report/all_transaction_report",transactionobj)
        }



}

