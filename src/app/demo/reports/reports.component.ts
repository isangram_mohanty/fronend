import { Component, OnInit } from "@angular/core";
import { ReportsService } from "./reports.service";


@Component({
    selector: 'app-report',
    templateUrl: './reports.component.html',
    styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {
    mainTransType: string;
    transactionType: string;
    operationPerformType: string;
    statusType: string;
    ReportTypeList: any;
    subcatList: any;
    operationPerformedList:any;
    transactionStatusList:any;
    
    
    constructor(private reportService: ReportsService) {}

    ngOnInit() {
        this.ReportTypeList = [
            "Recharge",
            "DMT",
            "AEPS",
            "mATM",
            "BBPS",
            "Insurance",
            "Cashout",
            "Commission",
            "Wallet",
            "UPI"
        ];
        this.subcatList = { 
                "Recharge" : [{name: "Recharge", value: "Recharge"}, {name: "Recharge New", value: "RechargeNew"}],
                "Commission" : [{name: "Commission1", value: "Commission"},{name: "Commission2", value: "COMMISSION2"}],
                'Wallet' : [
                    {name: "My Wallet1", value: "My Wallet"},
                    {name: "My Wallet2", value: "My Wallet2"},
                    {name: "Wallet Interchange", value: "WALLET_INTERCHANGE"}
                ],
                'DMT' : [
                    {name: 'All Transactions', value: "Money Transfer (All)"},
                    {name: 'Successful Transactions', value: "Money Transfer (Success)"},
                    {name: 'Refund Transactions', value: "Refund Transactions"}
                ],
                'AEPS' : [
                    {name: "AEPS1", value: "AEPS"},
                    {name: "AEPS2", value: "AEPS2"},
                ],
                "BBPS" : [{name: "BBPS", value: "BBPS"}],
                'mATM' : [
                    {name: "mATM", value: "iATM Settled"},
                    {name: "mATM2", value: "MATM2"}
                ],
                "Insurance" : [
                    {name: "Insurance", value: "Insurance"},
                    {name: "SAMPOORNA BIMA", value: "SAMPOORNA BIMA"}
                ],
                'Cashout': [
                    {name: "AEPS Cashout", value: "Wallet Cashout"},
                    {name: "mATM Cashout", value: "iATM Cashout Report"},
                    {name: "Wallet2 Cashout", value: "Wallet2_cashout"}
                ],
                "UPI": [{name: "UPI", value: "UPI"}]
        }
      const upiOperationList = ["All", "QR_COLLECT", "UPI_COLLECT", "QR_STATIC"];
      const rechargeOperationList = ["Prepaid Recharge"];
      const bbpsOperationList = ["All", "BBPS_Electricity","BBPS_Broadband Postpaid","BBPS_DTH","BBPS_Gas","BBPS_Landline Postpaid","BBPS_Mobile Postpaid","BBPS_Water","BBPS_Loan Repayment","BBPS_Life Insurance","BBPS_Fastag","BBPS_LPG Gas","BBPS_Cable TV","BBPS_Health Insurance","BBPS_Education Fees"];
      const aeps2OperationList = ["All", "AEPS_CASH_WITHDRAWAL", "AEPS_BALANCE_ENQUIRY", "AEPS_MINI_STATEMENT"];
      const matm2OperationList = ["All", "MATM2_BALANCE_ENQUIRY", "MATM2_CASH_WITHDRAWAL"];
      const statusAll = ["All", "INITIATED","PENDING","SUCCESS","FAILED"];
      const matm2statusAll = ["All", "INITIATED", "FAILED", "SUCCESS"];
      const rechargeNewStatusAll = ["All", "INITIATED","PENDING","SUCCESS","REFUNDED","FAILED"];
      this.operationPerformedList = {
          'BBPS' : bbpsOperationList.map(el => ({name: el.replace('BBPS_', ''), value: JSON.stringify(el !== 'All' ? [el] : bbpsOperationList.filter(el => el !== "All"))})),
          'AEPS2' : aeps2OperationList.map(el => ({name: el.replace('AEPS_', '').replace('_', ''), value: JSON.stringify(el !== 'All' ? [el] : aeps2OperationList.filter(el => el !== "All"))})),
          'MATM2' : matm2OperationList.map(el => ({name: el.replace('MATM2_', '').replace('_', ' '), value: JSON.stringify(el !== 'All' ? [el] : matm2OperationList.filter(el => el !== "All"))})),
          'RechargeNew' : rechargeOperationList.map(el => ({name: el, value: JSON.stringify(el !== 'All' ? [el] : rechargeOperationList.filter(el => el !== "All"))})),
          'UPI' : upiOperationList.map(el => ({name: el, value: JSON.stringify(el !== 'All' ? [el] : upiOperationList.filter(el => el !== "All"))}))
      }
      this.transactionStatusList = {
          "BBPS" : statusAll.map(el => ({name: el, value: el === 'All' ? el : JSON.stringify([el])})),
          "AEPS2" : statusAll.map(el => ({name: el, value: el === 'All' ? el : JSON.stringify([el])})),
          'MATM2' : matm2statusAll.map(el => ({name: el, value: el === 'All' ? el : JSON.stringify([el])})),
          'RechargeNew': rechargeNewStatusAll.map(el => ({name: el, value: el === 'All' ? el : JSON.stringify([el])})),
          'UPI': statusAll.map(el => ({name: el, value: el === 'All' ? el : JSON.stringify([el])}))
          // 'MATM2' : matm2statusAll.map(el => ({name: el.replace('_', ''), value: JSON.stringify(el !== 'All' ? [el] : matm2statusAll.filter(el => el !== "All"))})),
      }

      this.mainTransType = "Recharge";
      this.transactionType = this.ReportTypeList[0];
      this.operationPerformType = "";
      this.statusType = "";

        const authData = {
            username: 'iserveu',
            password: 'Bios@8895'
        };

        this.reportService.login(authData).subscribe(
            (res: any) => {
                console.log('Login Response: ', res);
            },
            (err: any) => {
                console.log('Login Error: ', err);
            }
        );
    }

    public changeMainType(): void {
        this.transactionType = this.subcatList[this.mainTransType][0].value;
        this.operationPerformType = "";
        this.statusType = "";
    }


}