import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class ReportsService {

    constructor(
        private http: HttpClient
    ) {}

    login(authData: any) {
        return this.http.post('https://secure.iserveu.tech/getlogintoken.json', authData);
    }
}